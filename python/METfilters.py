def MetFilterRDF(**kwargs):
    try:
        from Base.Filters.METfilters import MetFilterRDF as MetFilterRDF_new
        return MetFilterRDF_new(**kwargs)
    except ModuleNotFoundError:
        raise ModuleNotFoundError(
            "\n" + 100 * "*" +\
            "\nThe implementation of the MetFilterRDF module has been moved to "
            "https://gitlab.cern.ch/cms-phys-ciemat/event_filters. Please download this package into"
            "Base/Filters, recompile, and try again.\n"
            "In any case, it's preferrable to use the MetFilterRDF module inside"
            "Base.Filters instead." + 100 * "*")
