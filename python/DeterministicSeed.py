from analysis_tools.utils import import_root
import os

ROOT = import_root()


class DeterministicSeedRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")

        if self.isMC:
            if not os.getenv(f"_event_seed"):
                os.environ[f"_event_seed"] = "_event_seed"
                
                if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gSystem.Load("libBaseModules.so")

                base = "{}/{}/src/Base/Modules".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
                ROOT.gROOT.ProcessLine(".L {}/interface/DeterministicSeedInterface.h".format(base))

                ROOT.gInterpreter.Declare("""auto SEED = DeterministicSeedInterface();""")

    def run(self, df):
        if not self.isMC:
            return df, []

        df = df.Define("event_seed", "SEED.compute_seed("
                    "event, run, luminosityBlock, Pileup_nPU, nJet, nFatJet, "
                    "nSubJet, nMuon, nElectron, nTau, nSV, nGenJet,"
                    "Electron_jetIdx, Electron_seediPhiOriY, Muon_jetIdx, "
                    "Muon_nStations, Tau_jetIdx, Tau_decayMode, "
                    "Jet_nConstituents, Jet_nElectrons, Jet_nMuons)")

        return df, ["event_seed"]

def DeterministicSeedRDF(**kwargs):
    """
    Module to obtain a seed via a deterministic method.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: DeterministicSeedRDF
            path: Base.Modules.DeterministicSeed
            parameters:
                isMC: self.dataset.process.isMC

    """
    return lambda: DeterministicSeedRDFProducer(**kwargs)
