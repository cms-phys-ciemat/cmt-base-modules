#include "Base/Modules/interface/correctionWrapper.h"

MyCorrections::MyCorrections() {}

MyCorrections::MyCorrections(std::string filename, std::string correction_name) {
  if (filename == "None" || correction_name == "None") {} // allow for dummy
  else {
    auto csetEl = correction::CorrectionSet::from_file(filename);
    SF_ = csetEl->at(correction_name);
  }
}

double MyCorrections::eval(const std::vector<correction::Variable::Type>& values) {
  return SF_->evaluate(values);
}
