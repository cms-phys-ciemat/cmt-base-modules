/*
Interface for accessing Numpy's random number generators and configurable
bit generators via its C API. Usage example in main.cpp below.

Author: Marcel Rieger

Class rearranged by Jona Motta
*/

#include "Base/Modules/interface/NumpySFC64GeneratorInterface.h"

NumpyGenerator::NumpyGenerator(std::string bit_generator_name)
    : np_(nullptr),
        random_mod_(nullptr),
        gen_cls_(nullptr),
        bit_gen_cls_(nullptr),
        bit_gen_(nullptr),
        gen_(nullptr),
        gen_normal_(nullptr),
        gen_uniform_(nullptr),
        bit_generator_name_(bit_generator_name) {
    // initialize python if not already done
    if (!Py_IsInitialized()) {
        Py_Initialize();
    }

    // initialize numpy
    if (!PyArray_API) {
        _import_array();
        assert(PyArray_API);
    }

    // load module and class references
    np_ = PyImport_ImportModule("numpy");
    random_mod_ = PyObject_GetAttrString(np_, "random");
    gen_cls_ = PyObject_GetAttrString(random_mod_, "Generator");
    bit_gen_cls_ = PyObject_GetAttrString(random_mod_, bit_generator_name.c_str());
    assert(bit_gen_cls_ != nullptr);
}

NumpyGenerator::~NumpyGenerator() {
    // reset all objects, no need to reset function and module pointers
    reset(gen_);
    reset(bit_gen_);
    reset(bit_gen_cls_);
    reset(gen_cls_);
}

void NumpyGenerator::set_seed(uint64_t seed = 0) {
    reset(gen_normal_);
    reset(gen_uniform_);
    reset(gen_);
    reset(bit_gen_);

    // create bit generator
    npy_intp seed_dims[1] = {1};
    PyObject* np_seed = PyArray_SimpleNewFromData(1, seed_dims, NPY_UINT64, &seed);
    bit_gen_ = PyObject_CallFunctionObjArgs(bit_gen_cls_, np_seed, NULL);
    Py_DECREF(np_seed);

    // create generator
    gen_ = PyObject_CallFunctionObjArgs(gen_cls_, bit_gen_, NULL);

    // lookup methods
    gen_normal_ = PyObject_GetAttrString(gen_, "normal");
    gen_uniform_ = PyObject_GetAttrString(gen_, "uniform");
}

double NumpyGenerator::normal(size_t n, uint64_t seed = 0) {
    if (seed > 0) {
        set_seed(seed);
    }
    return next(gen_normal_, n);
}

double NumpyGenerator::uniform(size_t n, uint64_t seed = 0) {
    if (seed > 0) {
        set_seed(seed);
    }
    return next(gen_uniform_, n);
}

void NumpyGenerator::reset(PyObject*& obj) {
    if (obj != nullptr) {
        Py_DECREF(obj);
        obj = nullptr;
    }
}
    
double NumpyGenerator::next(PyObject*& gen_func, size_t n) {
    // all state objects must be initialized
    assert(gen_func != nullptr);

    // generate n random numbers, use the last one
    PyObject* ret = nullptr;
    for (size_t i = 0; i <= n; i++) {
        ret = PyObject_CallFunctionObjArgs(gen_func, NULL);
    }

    // cast and cleanup
    double value = PyFloat_AsDouble(ret);
    Py_DECREF(ret);

    return value;
}