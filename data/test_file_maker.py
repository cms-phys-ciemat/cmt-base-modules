import ROOT

input_files = {
    "mc2016_apv": "/store/mc/RunIISummer20UL16NanoAODAPVv9/GluGluToHHTo2B2Tau_TuneCP5_PSWeights_node_SM_13TeV-madgraph-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_preVFP_v11-v1/2520000/B7F6DA70-6DD8-E648-BD2B-F95B8AD3BBA6.root",
    "mc2016": "/store/mc/RunIISummer20UL16NanoAODv9/GluGluToHHTo2B2Tau_TuneCP5_PSWeights_node_SM_13TeV-madgraph-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_v17-v1/60000/09FDCB2B-989F-0E47-B476-D6348BE8EFAE.root",
    "mc2017": "/store/mc/RunIISummer20UL17NanoAODv9/GluGluToHHTo2B2Tau_TuneCP5_PSWeights_node_SM_13TeV-madgraph-pythia8/NANOAODSIM/106X_mc2017_realistic_v9-v1/2530000/593269DF-B896-DF49-9E43-2D7CDED032AF.root",
    "mc2018": "/store/mc/RunIISummer20UL18NanoAODv9/GluGluToHHTo2B2Tau_TuneCP5_PSWeights_node_SM_13TeV-madgraph-pythia8/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v1/2520000/0E84B0B2-C7F7-D142-8311-B431C65E82FA.root",
    "mc2022": "/store/mc/Run3Summer22NanoAODv12/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/40000/c1fa4f45-af71-4e4f-94a0-e4cea317f0f2.root",
    "mc2022_postee": "/store/mc/Run3Summer22EENanoAODv12/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v3/2540000/00e323ee-8d01-4701-a6e2-11d6b3b0aca8.root",
    "mc2023": "/store/mc/Run3Summer23NanoAODv12/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_v15-v2/2810000/017cdc55-9d9d-4ad5-8a89-e00f694f5c9f.root",
    "mc2023_postbpix": "/store/mc/Run3Summer23BPixNanoAODv12/GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_postBPix_v6-v2/40000/37e1e091-436a-4941-b264-d56c3858bfb0.root",

    "data2022": "/store/data/Run2022C/Tau/NANOAOD/22Sep2023-v1/40000/10629d52-92eb-45fd-a790-0d0fb0e4a232.root",
    "data2022_postee": "/store/data/Run2022E/Tau/NANOAOD/22Sep2023-v1/30000/117b7d01-bd3a-4f16-aaa2-9188b50a17d3.root",
    "data2023": "/store/data/Run2023C/Tau/NANOAOD/22Sep2023_v1-v2/2560000/57d7bd03-e7c8-497d-aa39-5a666e35dc59.root",
    "data2023_postbpix": "/store/data/Run2023D/Tau/NANOAOD/22Sep2023_v1-v1/2530000/7dd8ad6a-51dc-4632-8642-404e42818876.root"
}

for year, filename in input_files.items():
    print("root://xrootd-cms.infn.it://" + filename, "->", f"testfile_{year}.root")
    df = ROOT.RDataFrame("Events", "root://xrootd-cms.infn.it://" + filename)
    df = df.Range(0, 50)
    
    branches = [str(b) for b in df.GetColumnNames() if "L1_" not in str(b) and "HLT_" not in str(b)]
    df.Snapshot("Events", f"testfile_{year}.root", tuple(branches))
