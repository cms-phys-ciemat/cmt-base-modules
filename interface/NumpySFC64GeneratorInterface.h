#ifndef NumpySFC64GeneratorInterface_h
#define NumpySFC64GeneratorInterface_h

/*
Interface for accessing Numpy's random number generators and configurable
bit generators via its C API. Usage example in main.cpp below.

Author: Marcel Rieger

Class rearranged by Jona Motta
*/

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <iostream>
#include <sstream>
#include <string>
#include <Python.h>
#include <numpy/arrayobject.h>

class NumpyGenerator {
public:
    NumpyGenerator(std::string bit_generator_name);

    ~NumpyGenerator();

    void set_seed(uint64_t seed);
    double normal(size_t n, uint64_t seed);
    double uniform(size_t n, uint64_t seed);

private:
    PyObject* np_;
    PyObject* random_mod_;
    PyObject* gen_cls_;
    PyObject* bit_gen_cls_;
    PyObject* bit_gen_;
    PyObject* gen_;
    PyObject* gen_normal_;
    PyObject* gen_uniform_;
    std::string bit_generator_name_;

    void reset(PyObject*& obj);
    double next(PyObject*& gen_func, size_t n);
};

#endif // NumpySFC64GeneratorInterface_h